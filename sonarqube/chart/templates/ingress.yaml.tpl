apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/auth-realm: Authentication Required
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    nginx.ingress.kubernetes.io/auth-type: basic
    nginx.ingress.kubernetes.io/configuration-snippet: |
      proxy_set_header Authorization "Basic YWRtaW46YWRtaW4xMjM=";
  name: sonarqube
  namespace: sonarqube
spec:
  rules:
    - host: "sonarqube.{{ .Values.cluster_domain }}"
      http:
        paths:
          - backend:
              service:
                name: sonarqube
                port:
                  number: 80
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - "sonarqube.{{ .Values.cluster_domain }}"
      secretName: "{{ .Values.cluster_tls_wildcard_secret }}"
