#!/bin/bash

set -o errexit
set -o nounset
set -o pipefail

TMP="$(mktemp -d)"
trap "rm -fr '${TMP:?}'" EXIT

# generate the CRDs from the postgres-operator Helm chart
helmfile \
    --file=postgres-operator/helmfile.yaml \
    --selector=phase!=delete \
    --state-values-file=../ci/cluster.yaml \
    template \
    --include-crds \
    --output-dir-template='{{ .OutputDir }}' \
    --output-dir="${TMP:?}"

# verify that the expected "crds" directory exists
if ! test -d "${TMP:?}/postgres-operator/crds"; then
    exit 1
fi

# replace the old CRDs with the new CRDs
rm -r postgres-operator-crds/chart/templates
cp -r "${TMP:?}/postgres-operator/crds" postgres-operator-crds/chart/templates
